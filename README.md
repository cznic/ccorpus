# ccorpus

Package ccorpus provides a test corpus of C code.


## Installation

    $ go get modernc.org/ccorpus

## Documentation

[godoc.org/modernc.org/ccorpus](http://godoc.org/modernc.org/ccorpus)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fccorpus](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fccorpus)
